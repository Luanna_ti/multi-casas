<!DOCTYPE html>
<html lang="pt-br">
@include('layouts.head')

<body>

<div class="wrapper theme-2-active navbar-top-light">
@include('layouts.header')

@include('layouts.sidebar')

<!-- Main Content -->
    <div class="page-wrapper">
        <div class="container pt-30">
            <!-- Row -->
            <div class="row">
                @yield('content')
            </div>
            <!-- /Row -->

        </div>

        @include('layouts.footer')

    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->
@include('layouts.scripts')
</body>

</html>
